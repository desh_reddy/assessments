package smart.host.application.api.service;

import java.util.List;

import smart.host.application.room.RoomAllocation;

public interface IRoomOptimizationService {

	List<RoomAllocation> allocateRooms(int premiumAvailable, int economyAvailable, List<Integer> guestList);

}
