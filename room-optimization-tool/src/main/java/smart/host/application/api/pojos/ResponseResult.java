package smart.host.application.api.pojos;

public enum ResponseResult {
	SUCCESS, FAILED, INVALID
}
