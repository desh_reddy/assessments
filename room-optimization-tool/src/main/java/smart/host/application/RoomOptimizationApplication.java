package smart.host.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoomOptimizationApplication {

	public static void main(String[] args) {
		SpringApplication.run(RoomOptimizationApplication.class, args);
	}

}
