package smart.host.application.api.pojos;

public abstract class BaseRequest {

	protected String requestorId;

	public String getRequestorId() {
		return requestorId;
	}

	public void setRequestorId(String requestorId) {
		this.requestorId = requestorId;
	}

}
