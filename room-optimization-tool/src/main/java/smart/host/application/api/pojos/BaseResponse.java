package smart.host.application.api.pojos;

public abstract class BaseResponse {

	protected ResponseResult result;
	protected String resultMessage;

	public BaseResponse() {
	}

	public BaseResponse(ResponseResult result, String resultMessage) {
		this.result = result;
		this.resultMessage = resultMessage;
	}

	public ResponseResult getResult() {
		return result;
	}

	public void setResult(ResponseResult result) {
		this.result = result;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}

}
