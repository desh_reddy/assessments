
## Running the project

###Please note that to run this project you need to have Maven 3 installed.

###Step 1:
Open a terminal. Navigate to the project directory.

###Step 2:
Execute the command 
**mvn clean install** 
to build the project and execute the tests.

(To skip tests use **mvn clean install -DskipTests**)

###Step 3:
The project is deployed on the Spring Boot embedded Tomcat server.
Execute the command 
**mvn spring-boot:run**
to run the application.

The api services can now be consumed using the path *http://localhost:8080/api/allocate*

Postman was used to test the rest services.
An example of the request body (JSON):
```
{
	"requestorId": "P26X1",
	"economyCount" : 1,
	"premiumCount" : 2,
	"guestList" : [23,45,155,374,22,99,100,101,115,209]
}
```