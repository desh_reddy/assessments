package smart.host.application.api.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import smart.host.application.api.IRoomOptimizationController;
import smart.host.application.api.pojos.BaseResponse;
import smart.host.application.api.pojos.ResponseResult;
import smart.host.application.api.pojos.RoomOptimisationRequest;
import smart.host.application.api.pojos.RoomOptimisationResponse;
import smart.host.application.api.service.IRoomOptimizationService;
import smart.host.application.room.RoomAllocation;

@RestController
@RequestMapping(value = "/api")
public class RoomOptimizationController implements IRoomOptimizationController {

	@Autowired
	IRoomOptimizationService service;

	@Override
	@RequestMapping("/allocate")
	@ResponseBody
	public ResponseEntity<BaseResponse> allocate(@RequestBody(required = true) RoomOptimisationRequest request) {
		try {
			if (request.getGuestList().isEmpty()) {
				return ResponseEntity.ok(new RoomOptimisationResponse(ResponseResult.INVALID, "Guest list is empty."));
			}
			if (request.getPremiumCount() == 0 && request.getEconomyCount() == 0) {
				return ResponseEntity.ok(
						new RoomOptimisationResponse(ResponseResult.INVALID, "Please provide available room counts."));
			}
			List<RoomAllocation> result = service.allocateRooms(request.getPremiumCount(), request.getEconomyCount(),
					request.getGuestList());
			RoomAllocation premium = result.get(0);
			RoomAllocation economy = result.get(1);
			return ResponseEntity.ok(new RoomOptimisationResponse(ResponseResult.SUCCESS,
					premium.toString() + " " + economy.toString(), economy.getAllocatedCount(),
					premium.getAllocatedCount(), economy.getPriceTotal(), premium.getPriceTotal()));
		} catch (Exception e) {
			return ResponseEntity.ok(new RoomOptimisationResponse(ResponseResult.FAILED, e.getLocalizedMessage()));
		}

	}
}
