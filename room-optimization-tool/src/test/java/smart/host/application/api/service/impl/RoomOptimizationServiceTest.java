package smart.host.application.api.service.impl;

import static org.junit.Assert.assertTrue;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.json.JSONArray;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.TestContextManager;

import smart.host.application.api.service.IRoomOptimizationService;
import smart.host.application.room.RoomAllocation;

@RunWith(Parameterized.class)
public class RoomOptimizationServiceTest {
	@Parameter(value = 0)
	public int availablePremium;
	@Parameter(value = 1)
	public int availableEconomy;
	@Parameter(value = 2)
	public int resultPremiumCount;
	@Parameter(value = 3)
	public int resultPremiumTotal;
	@Parameter(value = 4)
	public int resultEconomyCount;
	@Parameter(value = 5)
	public int resultEconomyTotal;

	private TestContextManager testContextManager;

	List<Integer> guests;

	public void loadGuestData() throws Exception {
		guests = new ArrayList<>();
		ClassLoader classLoader = getClass().getClassLoader();
		String guestJsonData = new String(
				Files.readAllBytes(Paths.get(classLoader.getResource("smarthost_hotel_guests.json").toURI())));
		JSONArray jarr = new JSONArray(guestJsonData);
		for (int i = 0; i < jarr.length(); i++) {
			guests.add(jarr.getInt(i));
		}
		System.out.println("Guest test data loaded... " + guests.size() + " guests");
	}

	@Before
	public void setUp() throws Exception {
		this.testContextManager = new TestContextManager(getClass());
		this.testContextManager.prepareTestInstance(this);
		loadGuestData();
	}

	@TestConfiguration
	static class RoomOptimizationServiceImplTestContextConfiguration {

		@Bean
		public IRoomOptimizationService roomOptimizationService() {
			return new RoomOptimizationService();
		}
	}

	@Autowired
	private RoomOptimizationService roomOptimizationService;

	@Parameters
	public static Collection<Object[]> data() {
		Object[][] data = new Object[][] { { 3, 3, 3, 738, 3, 167 }, { 7, 5, 6, 1054, 4, 189 },
				{ 2, 7, 2, 583, 4, 189 }, { 7, 1, 7, 1153, 1, 45 } };
		return Arrays.asList(data);
	}

	@Test
	public void testAllocateRooms() {
		List<RoomAllocation> result = roomOptimizationService.allocateRooms(availablePremium, availableEconomy, guests);
		System.out.println("P: " +result.get(0).toString());
		System.out.println("E: " +result.get(1).toString());
		assertTrue(resultPremiumCount == result.get(0).getAllocatedCount()
				&& resultPremiumTotal == result.get(0).getPriceTotal()
				&& resultEconomyCount == result.get(1).getAllocatedCount()
				&& resultEconomyTotal == result.get(1).getPriceTotal());
	}

}
