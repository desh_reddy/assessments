package smart.host.application.api.pojos;

import java.io.Serializable;

public class RoomOptimisationResponse extends BaseResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7501420366444383156L;
	private int economyUsageCount;
	private int premiumUsageCount;
	private int economyUsageAmount;
	private int premiumUsageAmount;

	public RoomOptimisationResponse(ResponseResult result, String resultMessage) {
		this.result = result;
		this.resultMessage = resultMessage;
	}

	public RoomOptimisationResponse(ResponseResult result, String resultMessage, int economyUsageCount,
			int premiumUsageCount, int economyUsageAmount, int premiumUsageAmount) {
		super(result, resultMessage);
		this.economyUsageCount = economyUsageCount;
		this.premiumUsageCount = premiumUsageCount;
		this.economyUsageAmount = economyUsageAmount;
		this.premiumUsageAmount = premiumUsageAmount;
	}

	public int getEconomyUsageCount() {
		return economyUsageCount;
	}

	public void setEconomyUsageCount(int economyUsageCount) {
		this.economyUsageCount = economyUsageCount;
	}

	public int getPremiumUsageCount() {
		return premiumUsageCount;
	}

	public void setPremiumUsageCount(int premiumUsageCount) {
		this.premiumUsageCount = premiumUsageCount;
	}

	public int getEconomyUsageAmount() {
		return economyUsageAmount;
	}

	public void setEconomyUsageAmount(int economyUsageAmount) {
		this.economyUsageAmount = economyUsageAmount;
	}

	public int getPremiumUsageAmount() {
		return premiumUsageAmount;
	}

	public void setPremiumUsageAmount(int premiumUsageAmount) {
		this.premiumUsageAmount = premiumUsageAmount;
	}

}
