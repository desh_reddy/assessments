package smart.host.application.api.service.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.springframework.stereotype.Service;

import smart.host.application.api.service.IRoomOptimizationService;
import smart.host.application.room.RoomAllocation;
import smart.host.application.room.RoomCategory;

@Service
public class RoomOptimizationService implements IRoomOptimizationService {

	@Override
	public List<RoomAllocation> allocateRooms(int premiumAvailable, int economyAvailable, List<Integer> guestList) {

		Queue<Integer> premiumGuests = new LinkedList<>();
		Queue<Integer> economyGuests = new LinkedList<>();

		guestList.sort(Comparator.reverseOrder());
		for (Integer guestAmount : guestList) {
			if (guestAmount < RoomOptimizationConstants.PREMIUM_ECONOMY_THRESHOLD) {
				economyGuests.add(guestAmount);
			} else {
				premiumGuests.add(guestAmount);
			}
		}

		RoomAllocation premiumAllocation = new RoomAllocation(RoomCategory.PREMIUM, premiumAvailable, RoomOptimizationConstants.CURRENCY);
		RoomAllocation economyAllocation = new RoomAllocation(RoomCategory.ECONOMY, economyAvailable, RoomOptimizationConstants.CURRENCY);

		while (!premiumGuests.isEmpty() && premiumAllocation.getAvailableCount() > 0) {
			premiumAllocation.allocate(premiumGuests.remove());
		}

		if (premiumGuests.size() < premiumAvailable && economyGuests.size() > economyAvailable) {
			while (premiumAllocation.getAvailableCount() > 0 && !economyGuests.isEmpty()) {
				premiumAllocation.allocate(economyGuests.remove());
			}
		}

		while (!economyGuests.isEmpty() && economyAllocation.getAvailableCount() > 0) {
			economyAllocation.allocate(economyGuests.remove());
		}

		List<RoomAllocation> result = new ArrayList<>();
		result.add(premiumAllocation);
		result.add(economyAllocation);
		return result;
	}

}
