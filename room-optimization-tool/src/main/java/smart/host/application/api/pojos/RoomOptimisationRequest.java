package smart.host.application.api.pojos;

import java.io.Serializable;
import java.util.List;

public class RoomOptimisationRequest extends BaseRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2710321190178070964L;

	private int economyCount;
	private int premiumCount;
	private List<Integer> guestList;

	public int getEconomyCount() {
		return economyCount;
	}

	public void setEconomyCount(int economyCount) {
		this.economyCount = economyCount;
	}

	public int getPremiumCount() {
		return premiumCount;
	}

	public void setPremiumCount(int premiumCount) {
		this.premiumCount = premiumCount;
	}

	public List<Integer> getGuestList() {
		return guestList;
	}

	public void setGuestList(List<Integer> guestList) {
		this.guestList = guestList;
	}

}
