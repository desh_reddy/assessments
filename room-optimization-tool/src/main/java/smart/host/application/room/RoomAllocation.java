package smart.host.application.room;

public class RoomAllocation {
	RoomCategory roomCategory;
	int availableCount;
	int allocatedCount;
	int priceTotal;
	String currency;

	public RoomAllocation(RoomCategory roomCategory, int availableCount, String currency) {
		this.roomCategory = roomCategory;
		this.priceTotal = 0;
		this.allocatedCount = 0;
		this.availableCount = availableCount;
		this.currency = currency;
	}

	public void allocate(int price) {
		priceTotal += price;
		availableCount--;
		allocatedCount++;
	}

	public RoomCategory getRoomCategory() {
		return roomCategory;
	}

	public int getAvailableCount() {
		return availableCount;
	}

	public int getAllocatedCount() {
		return allocatedCount;
	}
	
	public int getPriceTotal() {
		return priceTotal;
	}

	@Override
	public String toString() {
		return "Usage "+ roomCategory + " : " + allocatedCount + " (" + currency + " "+ priceTotal + ");";
	}

}
