package smart.host.application.api.service.impl;

public class RoomOptimizationConstants {

	public static final String CURRENCY = "EUR";
	public static final int PREMIUM_ECONOMY_THRESHOLD = 100;

}
