package smart.host.application.api;

import org.springframework.http.ResponseEntity;

import smart.host.application.api.pojos.BaseResponse;
import smart.host.application.api.pojos.RoomOptimisationRequest;

public interface IRoomOptimizationController {

	ResponseEntity<BaseResponse> allocate(RoomOptimisationRequest request);

}
